# build contaiter
FROM node:10-alpine AS build

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY [ "package.json", "npm-shrinkwrap.json", "/usr/app/"]

# if you have any node-gyp modules
# RUN apk add --no-cache --virtual .build-deps make gcc g++ python \
#     && npm install --silent \
#     && apk del .build-deps

RUN npm install
COPY [ "./src", "/usr/app/src/" ]
COPY [ "./public", "/usr/app/public/" ]

RUN npm run build

# run contaier
FROM nginx:alpine

WORKDIR /usr/share/nginx/html
COPY --from=build /usr/app/build/ /usr/share/nginx/html/

EXPOSE 80