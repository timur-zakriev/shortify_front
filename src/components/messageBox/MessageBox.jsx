import React from 'react';
import './MessageBox.css';

const MessageBox = ({ message = '', isShowMessage = false }) =>{
    return (
        <div className={isShowMessage ? 'message-box' : 'message-box message-box_disable'}>{message}</div>
    )
}

export default MessageBox;