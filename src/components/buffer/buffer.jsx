import React from 'react';
import './buffer.css';


const Buffer = ({ onClick=f=>f }) => 
    <div onClick={onClick} className="buffer">Enter from buffer</div>


export default Buffer;